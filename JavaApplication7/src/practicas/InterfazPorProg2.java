/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package practicas;

//import java.awt.BorderLayout;//se encontro en otro paquete es de los mas antiguios
import Atxy2k.CustomTextField.RestrictedTextField;
//import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;// si pones* exporta todos los elementos de swing
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;//

/**
 *
 * @author W_W
 */
public class   InterfazPorProg2 extends JFrame{
    JButton btnAccion;
    JButton btnCerrar; 
    //JButton btn1;//comoponente para poder crear ventana con componente 
    JTextField tf1;
    JTextField tf2;
    //
     public static void main(String[] args) {
     InterfazPorProg2 frm = new InterfazPorProg2 ("Interfaaaaaaaaaaaaaaaaaaaz por pro programacion",640,480);//640 y 480 es tamaño  
     /*
     
     otra manera en la que pudo a ver quedado es 
     JFrame frm = new JFrame ("titulo");
     frm.setDefaultOperacion(JFrame.EXIT_ON_CLOSE);
     frm.setSIze(640,480);
     frm.setVisible(true);
     
    
     */
     frm.setVisible(true);
     
     }
     public InterfazPorProg2(String titulo, int ancho, int alto){//cosntrutor
     super(titulo); 
     this.setSize (ancho, alto);
     this.setLayout(new FlowLayout());
    
     this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);// lo cambiamos de lugar 
     
     this.btnAccion= new JButton("Accion");
     this.btnCerrar = new JButton("Cerrar");
     
     this.btnAccion.addActionListener(new ListenerBotonAccion());
     this.btnCerrar.addActionListener(new ListenerBotonCerrar());//va a mandar a llamar el metodo 
     
     this.tf1= new JTextField(40);
     this.tf2= new JTextField(10);
     
     
     this.add(new JLabel("Nombre"));
     this.add(tf1);
     this.add(new JLabel("Edad"));
     this.add(tf2); 
     this.add(btnAccion);
     this.add(btnCerrar);
     
     RestrictedTextField r  = new RestrictedTextField(tf2);
     r.setLimit(3);
     r.setOnlyNums(true);
  
             }
class ListenerBotonCerrar implements ActionListener{
    @Override
    public void actionPerformed(ActionEvent e) {
        System.exit(0);
        
    }

}
      class ListenerBotonAccion implements ActionListener{

    @Override
    public void actionPerformed(ActionEvent e) {
        String mensaje=""; 
        mensaje = tf1.getText()+"/"+ tf2.getText();
        JOptionPane.showMessageDialog(null, mensaje);
        
      }
    
}
}

